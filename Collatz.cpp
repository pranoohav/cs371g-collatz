// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <map>
#include "Collatz.hpp"

using namespace std;
int cache [500000];
const long size = 500000;

// ------------
// collatz_read
// ------------


pair<int, int> collatz_read (istream_iterator<int>& begin_iterator) {
    int i = *begin_iterator;
    ++begin_iterator;
    int j = *begin_iterator;
    ++begin_iterator;
    return make_pair(i, j);
}

// ------------
// helper
// ------------

//calculates cycle length of value and intermediate values
int helper(long num) {
    int length = 0;
    //reached end of cycle
    if (num == 1) {
        return 1;
    }
    //if already present in cache
    else if (num < size && cache[num] != 0) {
        return cache[num];
    }
    //if even
    else if (num % 2 == 0) {
        length = helper(num / 2) + 1;
    }
    //if odd
    else {
        length = helper(num + (num >> 1) + 1) + 2;
    }
    //add to cache if possible
    if(num < size) {
        cache[num] = length;
    }
    return length;
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i;
    int j;
    tie(i, j) = p;
    //check preconditions
    assert((i > 0) && (j > 0));
    assert((i < 1000000) && (j < 1000000));
    //figure out lower and upper bound values
    int upper = j;
    int lower = i;
    if (i > j) {
        upper = i;
        lower = j;
    }
    //in-class search range optimization
    if (lower < ((upper / 2) + 1)) {
        lower = (upper / 2) + 1;
    }
    int max = 1;
    //run through range
    for (int k = lower; k <= upper; k++) {
        //get cycle length
        int current = helper(k);
        //update max if needed
        if (current > max) {
            max = current;
        }
    }
    //check postcondition
    assert(max > 0);
    return make_tuple(i, j, max);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    //eager cache
    collatz_eval(pair<int,int>(2,500));
    istream_iterator<int> begin_iterator(sin);
    istream_iterator<int> end_iterator;
    while (begin_iterator != end_iterator) {
        collatz_print(sout, collatz_eval(collatz_read(begin_iterator)));
    }
}