# CS371g: Generic Programming Collatz Repo

* Name: Pranooha Veeramachaneni

* EID: pv5749

* GitLab ID: pranoohav

* HackerRank ID: pranooha

* Git SHA: be9d90c2c8c7151c2f78da487d47348765043d20

* GitLab Pipelines: https://gitlab.com/pranoohav/cs371g-collatz/-/pipelines

* Estimated completion time: 20

* Actual completion time: 18

* Comments: I hope I did everything correctly. I am very unfamiliar with a lot of these tools.
